package se.experis.SpringBootApplication.controllers;

import org.springframework.web.bind.annotation.*;

@RestController
public class HelloController {

    // Ennpoint that returns a greeting message along with the query parameter
    @RequestMapping(value = "/greeting", method = RequestMethod.GET)
    public String displayGreeting(@RequestParam("name") String displayName) {
        return "Hello " + displayName;
    }

    // Endpoint that reverses the req query parameter and returns it.
    @RequestMapping(value = "/reverse", method = RequestMethod.GET)
    public String reverseString(@RequestParam("name") String displayName) {

        byte [] strAsByteArray = displayName.getBytes();

        byte [] result = new byte [strAsByteArray.length];

        for (int i = 0; i<strAsByteArray.length; i++) {
            result[i] = strAsByteArray[strAsByteArray.length-i-1];
        }

        String reversedName = new String(result);

        return "Reversed name " + reversedName;
    }

}